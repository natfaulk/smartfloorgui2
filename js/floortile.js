const Savefiles = require('./savefiles.js')
const Common = require('./common.js')

let allTiles = null
let bb = null
Savefiles.init()

let setupTiles = (_settings, _layout) => {
  let temp = []
  let tiles = []
  _layout.forEach(_l => {
    if (_l.enabled) {
      temp.push(_l.pos)
      tiles.push(_l)
    }
  })

  // make it easier to figure out if this errors
  // errors later on if it doesnt error here hence why just print to console
  // Common.getBB will fail if length is 0
  if (temp.length === 0) console.log('Invalid layout as no tiles are enabled')
  bb = Common.getBB(temp)
  // console.log(bb)

  allTiles = new Floortile(
    _settings,
    tiles,
    bb
  )
}

let updateData = (_data) => {
  allTiles.update(_data)
}

let draw = _canvas => {
  if (allTiles !== null) {
    allTiles.draw(_canvas)
  }
}

let tare = () => {
  if (allTiles !== null) {
    allTiles.tare()
  }
}

let saveMatrix = () => {
  if (allTiles !== null) {
    Savefiles.write(JSON.stringify({
      time: Date.now(),
      data: allTiles.matrix,
      type: 'reading'
    }))
  }
}

let beginRecording = _prefix => {
  console.log('Started recording...')
  if (allTiles === null) return false
  Savefiles.nextFile(_prefix)
  
  let f = JSON.stringify
  if (typeof angular !== 'undefined') f = angular.toJson

  Savefiles.write(f({
    version: require('./../package.json').version,
    tiles: allTiles.tiles,
    bb: bb,
    type: 'header'
  }))
  return true
}

let endRecording = () => {
  console.log('Stopped recording')
  Savefiles.close()
}

let ID_MAP = {
  default: {
    p1 : [0, 0],
    p2 : [1, 0],
    p3 : [2, 0],
    p4 : [3, 0],
    p5 : [4, 0],
    p6 : [0, 1],
    p7 : [1, 1],
    p8 : [2, 1],
    p9 : [3, 1],
    p10: [4, 1],
    p11: [0, 2],
    p12: [1, 2],
    p13: [2, 2],
    p14: [3, 2],
    p15: [4, 2],
    p16: [0, 3],
    p17: [1, 3],
    p18: [2, 3],
    p19: [3, 3],
    p20: [4, 3],
    p21: [0, 4],
    p22: [1, 4],
    p23: [2, 4],
    p24: [3, 4],
    p25: [4, 4]
  },
  idOLD1: {
    p25: [0, 0],
    p20: [1, 0],
    p13: [2, 0],
    p6 : [3, 0],
    p1 : [4, 0],
    p24: [0, 1],
    p19: [1, 1],
    p14: [2, 1],
    p12: [3, 1],
    p2 : [4, 1],
    p23: [0, 2],
    p18: [1, 2],
    p15: [2, 2],
    p11: [3, 2],
    p3 : [4, 2],
    p22: [0, 3],
    p17: [1, 3],
    p7 : [2, 3],
    p10: [3, 3],
    p4 : [4, 3],
    p21: [0, 4],
    p16: [1, 4],
    p8 : [2, 4],
    p9 : [3, 4],
    p5 : [4, 4]
  },
  // idOLD3: {
  //   p5 : [0, 0],
  //   p9 : [1, 0],
  //   p8 : [2, 0],
  //   p16: [3, 0],
  //   p21: [4, 0],
  //   p4 : [0, 1],
  //   p10: [1, 1],
  //   p7 : [2, 1],
  //   p17: [3, 1],
  //   p22: [4, 1],
  //   p3 : [0, 2],
  //   p11: [1, 2],
  //   p15: [2, 2],
  //   p18: [3, 2],
  //   p23: [4, 2],
  //   p2 : [0, 3],
  //   p12: [1, 3],
  //   p14: [2, 3],
  //   p19: [3, 3],
  //   p24: [4, 3],
  //   p1 : [0, 4],
  //   p6 : [1, 4],
  //   p13: [2, 4],
  //   p20: [3, 4],
  //   p25: [4, 4]
  // },
  id0002: {
    p8 : [0, 0],
    p5 : [1, 0],
    p22: [2, 0],
    p19: [3, 0],
    p11: [4, 0],
    p9 : [0, 1],
    p10: [1, 1],
    p23: [2, 1],
    p13: [3, 1],
    p16: [4, 1],
    p3 : [0, 2],
    p20: [1, 2],
    p17: [2, 2],
    p14: [3, 2],
    p12: [4, 2],
    p4 : [0, 3],
    p21: [1, 3],
    p18: [2, 3],
    p15: [3, 3],
    p6 : [4, 3],
    p24: [0, 4],
    p25: [1, 4],
    p1 : [2, 4],
    p2 : [3, 4],
    p7 : [4, 4]
  },
  id0002_rot180: {
    p8 : [4, 4],
    p5 : [3, 4],
    p22: [2, 4],
    p19: [1, 4],
    p11: [0, 4],
    p9 : [4, 3],
    p10: [3, 3],
    p23: [2, 3],
    p13: [1, 3],
    p16: [0, 3],
    p3 : [4, 2],
    p20: [3, 2],
    p17: [2, 2],
    p14: [1, 2],
    p12: [0, 2],
    p4 : [4, 1],
    p21: [3, 1],
    p18: [2, 1],
    p15: [1, 1],
    p6 : [0, 1],
    p24: [4, 0],
    p25: [3, 0],
    p1 : [2, 0],
    p2 : [1, 0],
    p7 : [0, 0]
  },
  idNEW0: {
    p6 : [0, 0],
    p5 : [1, 0],
    p1 : [2, 0],
    p23: [3, 0],
    p21: [4, 0],
    p13: [0, 1],
    p7 : [1, 1],
    p11: [2, 1],
    p22: [3, 1],
    p20: [4, 1],
    p25: [0, 2],
    p16: [1, 2],
    p12: [2, 2],
    p24: [3, 2],
    p10: [4, 2],
    p17: [0, 3],
    p14: [1, 3],
    p8 : [2, 3],
    p3 : [3, 3],
    p4 : [4, 3],
    p18: [0, 4],
    p19: [1, 4],
    p15: [2, 4],
    p9 : [3, 4],
    p2 : [4, 4]
  },
  idNEW0_2: {
    p22: [0, 0],
    p21: [1, 0],
    p3 : [2, 0],
    p19: [3, 0],
    p6 : [4, 0],
    p23: [0, 1],
    p20: [1, 1],
    p2 : [2, 1],
    p14: [3, 1],
    p7 : [4, 1],
    p1 : [0, 2],
    p10: [1, 2],
    p9 : [2, 2],
    p18: [3, 2],
    p13: [4, 2],
    p11: [0, 3],
    p4 : [1, 3],
    p8 : [2, 3],
    p17: [3, 3],
    p25: [4, 3],
    p24: [0, 4],
    p12: [1, 4],
    p15: [2, 4],
    p5 : [3, 4],
    p16: [4, 4]
  },
  idNEW0_2_rot180: {
    p22: [4, 4],
    p21: [3, 4],
    p3 : [2, 4],
    p19: [1, 4],
    p6 : [0, 4],
    p23: [4, 3],
    p20: [3, 3],
    p2 : [2, 3],
    p14: [1, 3],
    p7 : [0, 3],
    p1 : [4, 2],
    p10: [3, 2],
    p9 : [2, 2],
    p18: [1, 2],
    p13: [0, 2],
    p11: [4, 1],
    p4 : [3, 1],
    p8 : [2, 1],
    p17: [1, 1],
    p25: [0, 1],
    p24: [4, 0],
    p12: [3, 0],
    p15: [2, 0],
    p5 : [1, 0],
    p16: [0, 0]
  },
  // idNEW0_rot90: {
  //   p18: [0, 0],
  //   p17: [1, 0],
  //   p25: [2, 0],
  //   p13: [3, 0],
  //   p6 : [4, 0],
  //   p19: [0, 1],
  //   p14: [1, 1],
  //   p16: [2, 1],
  //   p7 : [3, 1],
  //   p5 : [4, 1],
  //   p15: [0, 2],
  //   p8 : [1, 2],
  //   p12: [2, 2],
  //   p11: [3, 2],
  //   p1 : [4, 2],
  //   p9 : [0, 3],
  //   p3 : [1, 3],
  //   p24: [2, 3],
  //   p22: [3, 3],
  //   p23: [4, 3],
  //   p2 : [0, 4],
  //   p4 : [1, 4],
  //   p10: [2, 4],
  //   p20: [3, 4],
  //   p21: [4, 4]
  // }
}
ID_MAP.idOLD3 = ID_MAP.idOLD1
ID_MAP.idOLD2 = ID_MAP.idOLD1
ID_MAP.idOLD4 = ID_MAP.idOLD1
ID_MAP.id0002 = ID_MAP.id0002_rot180
ID_MAP.id0004 = ID_MAP.id0002
ID_MAP.id0005 = ID_MAP.id0002
ID_MAP.id0003 = ID_MAP.id0002

ID_MAP.idNEW0 = ID_MAP.idNEW0_2
ID_MAP.idNEW1 = ID_MAP.idNEW0
ID_MAP.idNEW2 = ID_MAP.idNEW0
ID_MAP.idNEW3 = ID_MAP.idNEW0

class Floortile {
  constructor(_settings, _tiles, _bb) {
    this.tiles = _tiles
    this.w = _bb.w * _tiles[0].dimensions.x
    this.h = _bb.h * _tiles[0].dimensions.y
    this.x = _bb.x * _tiles[0].dimensions.x
    this.y = _bb.y * _tiles[0].dimensions.y
    this.settings = _settings
    this.matrix = []
    
    this.tiles.forEach(_t => {
      _t.calibrated = false
      _t.calCount = 0
    })

    for (let x = 0; x < this.w; x++) {
      let t = []
      for (let y = 0; y < this.h; y++) {
        t.push({
          val: 0,
          offset: 0,
        })
      }

      this.matrix.push(t)
    }

    // console.log(this.matrix)
  }

  update(_data) {
    let tile = null
    // get id pos
    let nCalVals = this.settings.numCalVals

    this.tiles.forEach(_t => {
      if (_data.ID == _t.id) tile = _t
    })
    
    if (tile !== null) {
      let offset = {
        x: tile.pos.x * tile.dimensions.x,
        y: tile.pos.y * tile.dimensions.y
      }

      
      Object.keys(_data).forEach(elem => {
        if (elem != 'ID') {
          let pos = id_to_xy(_data.ID, elem)
          pos.x += (offset.x - this.x)
          pos.y += (offset.y - this.y)
          _data[elem] *= tile.prescale
          if (tile.calibrated) {
            this.matrix[pos.x][pos.y].val = _data[elem] - this.matrix[pos.x][pos.y].offset
          } else {
            this.matrix[pos.x][pos.y].offset += _data[elem] / this.settings.numCalVals
          }
        }
      })

      tile.calCount++
      if (tile.calCount >= nCalVals) {
        tile.calibrated = true
        tile.calCount = 0
      }
    } else {
      console.log(`No tile with ID: ${_data.ID}`)
    }
  }

  draw(_canvas) {
    let bb = {x: this.x, y: this.y, w: this.w, h: this.h}
    let _f = Common.draw
    if (this.settings.histo.enable) _f = Common.draw2
    _f(_canvas, this.matrix, this.settings, bb)
  }

  tare() {
    this.tiles.forEach(_t => {
      _t.calibrated = false
      _t.calCount = 0
    })

    this.matrix.forEach(x => {
      x.forEach(y => {
        y.val = 0
        y.offset = 0
      })
    })
  }
}

let id_to_xy = (_id, _p) => {
  let key = `id${_id}`
  if (!ID_MAP.hasOwnProperty(key)) key = 'default'
  let pos = ID_MAP[key][_p]
  return {x: pos[0], y: pos[1]}
}

module.exports = {
  setupTiles,
  updateData,
  draw,
  tare,
  saveMatrix,
  beginRecording,
  endRecording
}
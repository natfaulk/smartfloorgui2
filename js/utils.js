const fs = require('fs')

module.exports = {
  mkdir_p
}

function mkdir_p(_dir) {
  if (!fs.existsSync(_dir)) {
    fs.mkdirSync(_dir)
  }
}
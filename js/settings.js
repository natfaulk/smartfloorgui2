const fs = require('fs')
const path = require('path')

const SETTINGS_FILENAME = 'settings.json'

const DEFAULT_CAL_VALS              = 10
const DEFAULT_MAX_READING_VAL       = 1000
const DEFAULT_INTERP_FACTOR         = 5
const DEFAULT_FD_ENABLE             = true
const DEFAULT_FD_THRESHOLD          = 100
const DEFAULT_HISTO_ENABLE          = false
const DEFAULT_HISTO_ASCALE          = false

let _loadSettingsFile = () => {
  let setsFilepath = path.join('config', SETTINGS_FILENAME)

  if (fs.existsSync(setsFilepath)) {
    console.log('Found settings file. Loading...')
    let data = fs.readFileSync(setsFilepath)
    return JSON.parse(data)
  }

  console.log('No settings file found')  
  let output = newSettingsFile()
  saveSettingsFile(output)
  return output
}

let newSettingsFile = () => {
  console.log('Creating new settings file')  
  return {
    numCalVals: DEFAULT_CAL_VALS,
    max: DEFAULT_MAX_READING_VAL,
    interpFactor: DEFAULT_INTERP_FACTOR,
    fd: {
      enable: DEFAULT_FD_ENABLE,
      threshold: DEFAULT_FD_THRESHOLD
    },
    histo: {
      enable: DEFAULT_HISTO_ENABLE,
      autoscale: DEFAULT_HISTO_ASCALE
    }
  }
}

let saveSettingsFile = _obj => {
  console.log('Saving settings file...')  
  let setsFilepath = path.join('config', SETTINGS_FILENAME)
  _obj.version = require('./../package.json').version
  // in headless we dont have angular loaded so use JSON.stringify instead
  if (typeof angular !== 'undefined') fs.writeFileSync(setsFilepath, angular.toJson(_obj))
  else fs.writeFileSync(setsFilepath, JSON.stringify(_obj))
} 

module.exports = {
  load: _loadSettingsFile,
  save: saveSettingsFile
}

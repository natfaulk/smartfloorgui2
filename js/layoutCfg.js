// currently no error checking
// TODO: verify loaded files

const fs = require('fs')
const path = require('path')

const CFG_FILENAME = 'layout.json'

let loadCfgFile = () => {
  let cfgFilepath = path.join('config', CFG_FILENAME)

  if (fs.existsSync(cfgFilepath)) {
    console.log('Found config file. Loading...')
    let data = fs.readFileSync(cfgFilepath)
    return JSON.parse(data)
  }

  console.log('No config file found')  
  let output = newCfgFile()
  saveCfgFile(output)
  return output
}

let newCfgFile = () => {
  console.log('Creating new config file')  
  let out = []
  for (let i = 0; i < 16; i++) {
    out.push({
      enabled: false,
      id: "0000",
      dimensions: {
        x: 5,
        y: 5
      },
      pos: {
        x: i % 4,
        y: Math.floor(i / 4)
      },
      prescale: 1
    })
  }

  // Causes problems if no tiles are enabled
  // at some point should handle all empty tiles more gracefully
  out[0].enabled = true
  return out
}

let saveCfgFile = _obj => {
  console.log('Saving config file...')  
  let cfgFilepath = path.join('config', CFG_FILENAME)
  // in headless we dont have angular loaded so use JSON.stringify instead
  if (typeof angular !== 'undefined') fs.writeFileSync(cfgFilepath, angular.toJson(_obj))
  else fs.writeFileSync(cfgFilepath, JSON.stringify(_obj))
} 

module.exports = {
  loadCfgFile,
  newCfgFile,
  saveCfgFile,
  CFG_FILENAME
}

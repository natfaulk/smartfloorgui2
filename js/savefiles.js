const fs = require('fs')
const path = require('path')
const OS = require('os')
const mkdir_p = require('./utils').mkdir_p

const EXTENSION = 'log'


module.exports = {
  _initCalled: false,  
  _outputDir: 'outputs',
  _currentFile: null,
  init: (_dir) => {
    if (_dir !== undefined) module.exports._outputDir = _dir
    mkdir_p(module.exports._outputDir)
    module.exports._initCalled = true
  },
  nextFile: (prefix) => {
    if (module.exports._initCalled)
    {
      if (typeof prefix === 'undefined') prefix = ''
      else if (prefix !== '') prefix += '_'
  
      let i = 0
      let current_files = fs.readdirSync(module.exports._outputDir)
      while(current_files.indexOf(`${prefix}${i}.${EXTENSION}`) >= 0) i++
      let filename = `${prefix}${i}.${EXTENSION}`

      module.exports._currentFile = fs.createWriteStream(
        path.join(module.exports._outputDir, filename),
        (err) => {
          if (err) console.log('Error creating write stream')
      })

      return i
    } else console.log('Savefiles not initiated')
  },
  write: (_data) => {
    if (module.exports._currentFile !== null) {
      module.exports._currentFile.write(_data)
      module.exports._currentFile.write(OS.EOL)
    } else console.log('Error no file open')
  },
  close: (_callback) => {
    if (module.exports._currentFile !== null)
    {
      module.exports._currentFile.on('finish', () => {
        console.log('File has been written');
      })
      module.exports._currentFile.end()
    }
  }
}

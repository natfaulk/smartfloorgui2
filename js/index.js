const express = require('express')
const Floortiles = require('./floortile')
const Mindrawing = require('mindrawingjs')
const Settings = require('./settings')
const mkdir_p = require('./utils').mkdir_p

const ex_app = express()
const port = 3000

let myApp = angular.module('myApp', [])
myApp.controller('display', ['$scope', '$interval', function($s, $interval) {
  $s.currentpage = 'display'
  $s.maxval = '-1'
  $s.recording = {
    enabled: false,
    prefix: 'out'
  }
  $s.VERSION = require('./../package.json').version

  mkdir_p('config')
  $s.settings = Settings.load()

  $s.$watch('settings', (newVal, oldVal) => {
    Settings.save($s.settings)
  }, true)

  ex_app.use(express.json())
  ex_app.use(function (err, req, res, next) {
    if (err) {
      console.log(err)
      console.log(req)
      res.send('nack')
    }
    else next()
  })

  // ex_app.use(logger('dev'))
  ex_app.get('/', (req, res) => res.send('Hello World!'))
  ex_app.post('/submit', (req, res) => {
    // console.log('this ran')
    // console.log(req.body)

    let id = req.body.ID
    // console.log(id)

    // floortile.update(req.body)
    // console.log(req.body)
    Floortiles.updateData(req.body)

    if ($s.recording.enabled) Floortiles.saveMatrix()

    // $s.$apply()
    // $s.draw()
    res.send('ack')
  })
  ex_app.listen(port, '0.0.0.0', () => console.log(`Example app listening on port ${port}!`))
  
  // used to populate the canvasses in the html
  $s.canvases = []

  $s.d = new Mindrawing()
  $s.d.setup('display')
  let rect = $s.d.c.parentNode.getBoundingClientRect()
  let tempSize = 1280
  // let tempSize = Math.min(rect.width, rect.height)
  $s.d.setCanvasSize(tempSize, tempSize)
  $s.d.background('black')

  setInterval(() => {
    // $s.$apply()
    $s.draw()
  }, 105)

  $s.draw = () => {
    Floortiles.draw($s.d)
  }  

  $s.startRecord = () => {
    $s.recording.enabled = Floortiles.beginRecording($s.recording.prefix)
  }

  $s.finishRecord = () => {
    $s.recording.enabled = false
    Floortiles.endRecording()
  }

  $s.tare = () => {
    Floortiles.tare()
  }
}])


const mkdir_p = require('./utils').mkdir_p
const Floortiles = require('./floortile')
const LayoutCfg = require('./layoutCfg')

angular.module('myApp')
.controller('layout', ['$scope', '$interval', function($s, $interval) {
  mkdir_p('config')

  $s.squares = LayoutCfg.loadCfgFile()
  console.log($s.squares)
  
  $s.saveLayout = () => {
    // if no tiles are enabled refuse to save as will cause errors
    let tilesEnabled = false
    $s.squares.forEach(_tile => {
      if (_tile.enabled) tilesEnabled = true
    })

    if (tilesEnabled) {
      LayoutCfg.saveCfgFile($s.squares)
      Floortiles.setupTiles($s.$parent.settings, $s.squares)
      // clear background in case shape changes
      $s.$parent.d.background('black')
    } else console.log('Failed to save - please enable at least one tile')
  }
  $s.saveLayout()
}])

const fs = require('fs')
const path = require('path')
const readline = require('readline')
const Mindrawing = require('mindrawingjs')
const Common = require('./common.js')

angular.module('myApp')
.controller('replay', ['$scope', '$interval', function($s, $interval) {
  $s.savedFiles = []
  $s.currentFile = ''
  $s.autoscale = {
    min: 0,
    max: 0,
    init: false
  }
  $s.dataheader = {}
  $s.data = []
  $s.data_i = 0
  let basePath = path.join(__dirname, '..', 'outputs') 

  $s.getSavedFiles = () => {
    fs.readdir(basePath, (err, _files) => {
      if (err) {
        console.log(err)
        return
      }
      
      let filesOut = []
      _files.forEach(_file => {
        if (fs.statSync(path.join(basePath, _file)).isFile()) {
          filesOut.push(_file)
        }
      })

      $s.savedFiles = filesOut
      $s.$apply()
    })
  }
  $s.getSavedFiles()

  $s.openFile = _f => {
    console.log(`Opening ${_f}`)
    $s.data = []
    $s.data_i = 0  

    let rl = readline.createInterface({
      input: fs.createReadStream(path.join(basePath, _f))
    })

    rl.on('line', _line => {
      let parseddata = {}
      try {
        parseddata = JSON.parse(_line)
      } catch(e) {
        console.log('Error parsing file')
      }
      if (parseddata !== {}) {
        if (parseddata.type === 'header') {
          $s.dataheader = parseddata
          console.log(`Save file version: ${parseddata.version}`)
        }

        if (parseddata.type === 'reading') {
          $s.data.push(parseddata)
          
          parseddata.data.forEach(e => {
            e.forEach(e2 => {
              if (e2.val < $s.autoscale.min) $s.autoscale.min = e2.val
              if (e2.val > $s.autoscale.max) $s.autoscale.max = e2.val
            })
          })
        }
      }
    })

    rl.on('close', () => {
      $s.currentFile = _f
      $s.autoscale.init = true
      $s.$apply()

      $s.d_r = new Mindrawing()
      $s.d_r.setup('replay-canvas')
      let rect = $s.d_r.c.parentNode.getBoundingClientRect()
      let tempSize = Math.min(rect.width, rect.height - 50)
      console.log(rect.width, rect.height, tempSize)
      $s.d_r.setCanvasSize(tempSize, tempSize)
      $s.d_r.background('black')

      $s.updatePlot()
    })
  }

  $s.updatePlot = () => {
    let _f = Common.draw
    if ($s.$parent.settings.histo.enable) _f = Common.draw2
    _f($s.d_r, $s.data[$s.data_i].data, $s.$parent.settings, $s.dataheader.bb)
  }
}])

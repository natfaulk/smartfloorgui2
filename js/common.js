const interp2d = require('./interp.js').interp2d
const COV = require('compute-covariance')
const Numeric = require('numeric')

let _mean = (_list) => {
  let sum = 0
  _list.forEach(val => {
    sum += val
  })
  return sum / _list.length
}

let _normalise = (_m, _max) => {
  for (let i = 0; i < _m.length; i++) {
    for (let j = 0; j < _m[i].length; j++) {
      _m[i][j] *= (256 / _max)
    }
  }
}

let _threshold = (_m, _thresh) => {
  for (let i = 0; i < _m.length; i++) {
    for (let j = 0; j < _m[i].length; j++) {
      if (_m[i][j] > _thresh) _m[i][j] = 255
      else _m[i][j] = 0
    }
  }
}

let _toMatrix = (_m) => {
  let tempBuff = []
  _m.forEach(e_x => {
    let t = []
    e_x.forEach(e_y => {
      t.push(e_y.val)
    })
    tempBuff.push(t)
  })
  return tempBuff
}

let _findBlobs = _matrix => {
  let visited = new Set()
  let blobs = []
  
  const t_width = _matrix.length
  const t_height = _matrix[0].length
  
  const setKey = (_x, _y) => _x + _y * t_width
  
  for (let x = 0; x < t_width; x++) {
    for (let y = 0; y < t_height; y++) {
      if (!visited.has(setKey(x, y))) {
        
        if (_matrix[x][y] == 0) visited.add(setKey(x, y))
        else {
          let _blob = _getConnected(_matrix, x, y)
          blobs.push(_blob)
          _blob.forEach(_b => {
            visited.add(setKey(_b.x, _b.y))
          })
        }
      }
    }
  }
  
  return blobs
}

let _getConnected = (_matrix, x, y) => {
  const t_width = _matrix.length
  const t_height = _matrix[0].length

  let toSearch = [{x, y}]
  let out = []

  const setKey = (_x, _y) => _x + _y * t_width
  let visited = new Set([setKey(x, y)])

  while (toSearch.length > 0) {
    let temp = toSearch.pop()
    out.push(temp)

    if (
      !visited.has(setKey(temp.x, temp.y - 1))
      && (temp.y - 1) >= 0
      && _matrix[temp.x][temp.y - 1] > 0
      ) toSearch.push({x: temp.x, y: temp.y - 1})

    if (
      !visited.has(setKey(temp.x, temp.y + 1))
      && (temp.y + 1) < t_height
      && _matrix[temp.x][temp.y + 1] > 0
      ) toSearch.push({x: temp.x, y: temp.y + 1})

    if (
      !visited.has(setKey(temp.x - 1, temp.y))
      && (temp.x - 1) >= 0
      && _matrix[temp.x - 1][temp.y] > 0
      ) toSearch.push({x: temp.x - 1, y: temp.y})

    if (
      !visited.has(setKey(temp.x + 1, temp.y))
      && _matrix[temp.x + 1][temp.y] > 0
      && (temp.x + 1) < t_width
      ) toSearch.push({x: temp.x + 1, y: temp.y})

    visited.add(setKey(temp.x, temp.y - 1))
    visited.add(setKey(temp.x, temp.y + 1))
    visited.add(setKey(temp.x - 1, temp.y))
    visited.add(setKey(temp.x + 1, temp.y))
  }

  return out
}

// get blob Bounding Box
// returns {x, y, w, h}
let _getBlobBB = _blob => {
  let xmin = _blob[0].x
  let xmax = xmin
  let ymin = _blob[0].y
  let ymax = ymin

  for (let i = 1; i < _blob.length; i++) {
    if (_blob[i].x < xmin) xmin = _blob[i].x
    if (_blob[i].x > xmax) xmax = _blob[i].x
    if (_blob[i].y < ymin) ymin = _blob[i].y
    if (_blob[i].y > ymax) ymax = _blob[i].y
  }

  return {x: xmin, y: ymin, w: (xmax - xmin) + 1, h: (ymax - ymin) + 1}
}

let _draw = (_canvas, _matrix, _settings, _layoutbb) => {
  let m = _toMatrix(_matrix)
  m = interp2d(m, _settings.interpFactor)
  _normalise(m, _settings.max)
  
  _drawToCanvas(_canvas, m, _layoutbb)
  
  if (_settings.fd.enable) {
    let feet = _footDetect(m, _settings.fd.threshold)
    _drawFeet(_canvas, m, feet, _layoutbb)
  }
}


// returns object with:
// {
//   blob: array of all the points in {x: xpos, y: ypos} format
//   bb: bounding box as an object {x: xpos, y: ypos, w: width, h: height}
//   com: centre of mass of the blob in {x: xpos, y: ypos} format
//   evecs: eigen vectors of the covariant matrix - tells the direction, in a 2x2 array format
//            [[v1, v2], [v3, v3]]
// }
let _footDetect = (_matrix, _threshVal) => {
  _threshold(_matrix, _threshVal)

  let blobs = _findBlobs(_matrix)
  let out = []

  blobs.forEach(_blob => {
    let tempOut = {blob: _blob}
    // get bounding boxe
    tempOut.bb = _getBlobBB(_blob)

    // convert from objects into arrays
    let xy = [[], []]
    _blob.forEach(_pt => {
      xy[0].push(_pt.x)
      xy[1].push(_pt.y)
    })

    let mean = {x:_mean(xy[0]), y:_mean(xy[1])}
    for (let i = 0; i < xy[0].length; i++) xy[0][i] -= mean.x
    for (let i = 0; i < xy[1].length; i++) xy[1][i] -= mean.y
    // get covariant matrix
    let cov = COV(xy[0], xy[1])
    // eigenvectors
    tempOut.evecs = Numeric.eig(cov).E.x

    tempOut.com = {
      x: mean.x,
      y: mean.y
    }

    out.push(tempOut)
  })

  return out
}

let _drawFeet = (_canvas, _matrix, _feet, _layoutbb) => {
  let tWidth = _canvas.width / _matrix.length
  let tHeight = _canvas.height / _matrix[0].length

  let temp = _layoutbb.w / _layoutbb.h
  if (temp > 1) tHeight /= temp
  if (temp < 1) tWidth *= temp

  _feet.forEach(_foot => {
    // bounding box
    _canvas.stroke('green')
    _canvas.fill('transparent')
    _canvas.rect(_foot.bb.x*tWidth, _foot.bb.y*tHeight, _foot.bb.w*tWidth, _foot.bb.h*tHeight)

    // COM
    _canvas.stroke('red')
    _canvas.ellipse(_foot.com.x*tWidth, _foot.com.y*tHeight, 10)

    // direction
    _canvas.line(
      _foot.com.x*tWidth - 5*tWidth *_foot.evecs[0][0],
      _foot.com.y*tHeight - 5*tWidth * _foot.evecs[1][0],
      _foot.com.x*tWidth + 5*tWidth * _foot.evecs[0][0],
      _foot.com.y*tHeight + 5*tWidth * _foot.evecs[1][0]
    )

    _canvas.line(
      _foot.com.x*tWidth - 2*tWidth * _foot.evecs[0][1],
      _foot.com.y*tHeight - 2*tWidth * _foot.evecs[1][1],
      _foot.com.x*tWidth + 2*tWidth * _foot.evecs[0][1],
      _foot.com.y*tHeight + 2*tWidth * _foot.evecs[1][1]
    )
  })
}

let _drawToCanvas = (_canvas, _matrix, _layoutbb) => {
  let tWidth = _canvas.width / _matrix.length
  let tHeight = _canvas.height / _matrix[0].length
  
  let temp = _layoutbb.w / _layoutbb.h
  if (temp > 1) tHeight /= temp
  if (temp < 1) tWidth *= temp

  for (let x = 0; x < _matrix.length; x++) {
    for (let y = 0; y < _matrix[0].length; y++) {
      let t = Math.round(_matrix[x][y])
      if (t < 0) t = 0
      if (t > 255) t = 255
      _canvas.stroke(`rgb(${t},${t},${t})`)
      _canvas.fill(`rgb(${t},${t},${t})`)
      _canvas.rect(Math.round(x * tWidth) - 0.5, Math.round(y * tHeight) - 0.5, tWidth + 1, tHeight + 1)
    }
  }
}

module.exports = {
  draw: _draw,
  draw2: (_canvas, _matrix, _settings) => {
    _canvas.background('black')
    let m = _toMatrix(_matrix)
    m = interp2d(m, _settings.interpFactor)
    _normalise(m, _settings.max)
    
    let histo = []
    for (let i = 0; i < 64; i++) histo.push(0)

    for (let x = 0; x < m.length; x++) {
      for (let y = 0; y < m[0].length; y++) {
        let t = Math.round(m[x][y])
        if (t < 0) t = 0
        if (t > 255) t = 255
        
        histo[Math.floor(t/4)]++
        
      }
    }

    _canvas.stroke('white')
    _canvas.fill('white')

    histo.forEach((_v, _i) => {
      let t_h = 0.9 * _v * _canvas.height 
      if (_settings.histo.autoscale) t_h /= Math.max(...histo)
      else t_h /= (m.length * m[0].length)
      _canvas.rect(
        _i * _canvas.width / histo.length,
        _canvas.height - t_h,
        _canvas.width / histo.length,
        t_h
        )
    })

  },
  getBB: _getBlobBB,
  toMatrix: _toMatrix,
  normalise: _normalise,
  footDetect: _footDetect
}

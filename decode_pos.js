const fs = require('fs')
const path = require('path')
const readline = require('readline')
const Settings = require('./js/settings')
const Common = require('./js/common')
const mkdir_p = require('./js/utils').mkdir_p
const interp2d = require('./js/interp.js').interp2d

const VERSION = require('./package.json').version

;(() => {
  let args = process.argv.slice(2)
  if (args.length < 1) {
    console.log('Please add a filename as an arguement')
    return
  }
  let filename = args[0]
  if (filename.endsWith('.log')) filename = filename.slice(0, filename.length - 4)
  
  console.log(`Application version: ${VERSION}`)
  console.log('Loading settings...')
  mkdir_p('config')
  mkdir_p('processed')
  let settings = Settings.load()

  console.log(`Opening ${filename}.log`)
  let fpath = path.join(__dirname, 'outputs', filename + '.log')

  let data = []
  let dataheader = null
  let out = []

  let rl = readline.createInterface({
    input: fs.createReadStream(fpath)
  })

  rl.on('line', _line => {
    let parseddata = {}
    try {
      parseddata = JSON.parse(_line)
    } catch(e) {
      console.log('Error parsing file')
    }
    if (parseddata !== {}) {
      if (parseddata.type === 'header') {
        dataheader = parseddata
        console.log(`Save file version: ${parseddata.version}`)
      }

      if (parseddata.type === 'reading') data.push(parseddata)
    }
  })

  rl.on('close', () => {
    console.log(`Loaded ${data.length} readings`)

    data.forEach(_d => {
      let m = Common.toMatrix(_d.data)
      m = interp2d(m, settings.interpFactor)
      Common.normalise(m, settings.max)

      let feet = Common.footDetect(m, settings.fd.threshold)
      console.log(`Found ${feet.length} feet`)
      feet.forEach(_foot => {
        out.push({
          time: _d.time,
          com: _foot.com,
          angle: Math.atan(_foot.evecs[1][0] / _foot.evecs[0][0])
        })
      })
    })

    fs.writeFileSync(path.join(__dirname, 'processed', filename + '.json'), JSON.stringify(out))
  })
})()


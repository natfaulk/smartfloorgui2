// use http over https because that is what the ESP8266s on the devices use
const http = require('http')

let trimArg = _arg => {
  return _arg.substring(_arg.indexOf('=') + 1)
}

let displayHelp = () => {
  console.log('\nMock device help')
  console.log('----------------\r\n')
  console.log('Set id:')
  console.log('--id=123 where 123 is the id. Defaults to 0\n')
  console.log('Set grid dimensions:')
  console.log('--size=wxh where w is the width and h is the height. Defaults to 6x6\n')
  console.log('Set data interval')
  console.log('--interval=5000 data send interval in milliseconds. Defaults to 5000ms\n')
  console.log('Set host address:')
  console.log('--host=127.0.0.1 Defaults to 127.0.0.1')
  console.log('Can use domain name ie test.com or ip')
  console.log('Note this input is not validated. If there is a server error check this parameter\n')
  console.log('Set port:')
  console.log('--port=8080 Defaults to 8080\n')
  console.log('Display help:')
  console.log('help or --help or -h\n')
}

let getSendData = settings => {
  return () => {
    let output = {
      ID: settings.id
    }
    
    for (let i = 1; i <= settings.size.w*settings.size.h; i++) {
      output[`p${i}`] = Math.floor(2000 * Math.random())
    }
    
    let postData = JSON.stringify(output)

    console.log(`Sending data to ${settings.dest}`)
    // console.log(`Message: ${postData}`)
    
    let options = {
      host: settings.dest,
      path: '/submit',
      port: settings.port,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(postData)
      }
    }

    let req = http.request(options, res => {
      let data = ''

      res.on('data', chunk => {
        data += chunk
      })

      res.on('end', () => {
        console.log(data);
      })
    }).on('error', err => {
      console.log(`Error: ${err.message}`)
    })

    req.write(postData)
  }
}

(() => {
  let settings = {
    id: 0,
    size: {w: 6, h: 6},
    dest: '127.0.0.1',
    port: 8080,
    interval: 5000
  }

  let exitProgram = false

  process.argv.slice(2).forEach(arg => {
    if (arg.startsWith('--id=')) {
      settings.id = trimArg(arg)
      console.log(`ID set to ${settings.id}`)
    } else if (arg.startsWith('--size=')) {
      let tempSplit = trimArg(arg).split('x')
      if (tempSplit.length < 2) {
        console.log(`Invalid dimension - ${trimArg(arg)} Ignoring...`)
      } else {
        let tempSize = {w:parseInt(tempSplit[0]), h:parseInt(tempSplit[1])}

        if (isNaN(tempSize.w)) console.log(`Width not a number. Ignoring dimensions...`)
        else if (isNaN(tempSize.h)) console.log(`Height not a number. Ignoring dimensions...`)
        else {
          settings.size = tempSize
          console.log(`Grid dimensions set to ${settings.size.w} x ${settings.size.h}`)
        }
      }
    } else if (arg.startsWith('--host=')) {
      settings.dest = trimArg(arg)
      console.log(`Host address set to ${settings.dest}`)
    } else if (arg.startsWith('--port=')) {
      let tempPort = parseInt(trimArg(arg))
      if (isNaN(tempPort)) console.log(`Port not a number. Ignoring...`)
      else {
        settings.port = tempPort
        console.log(`Port set to ${settings.port}`)
      }
    } else if (arg.startsWith('--interval=')) {
      let tempInterval = parseInt(trimArg(arg))
      if (isNaN(tempInterval)) console.log(`Interval not a number. Ignoring...`)
      else {
        settings.interval = tempInterval
        console.log(`Interval set to ${settings.interval}`)
      }
    } else if (arg == 'help' || arg == '--help' || arg == '-h') {
      displayHelp()
      exitProgram = true
    } else console.log(`Unknown argument: ${arg}`)
  })

  if (exitProgram) return

  setInterval(getSendData(settings), settings.interval)
})()


const logger = require('@natfaulk/supersimplelogger')('Process all')
const reshape = require('./reshape')
const calumise = require('./calumise')
const fs = require('fs')
const path = require('path')

;(()=>{
  logger('RECCOMEND TO RUN WITH INCREASED HEAP SIZE')
  logger('eg node --max-old-space-size=4096 process_all')

  logger('Loading filenames')
  
  const SF_PATH = path.join(__dirname, '..', 'outputs')

  allFiles = fs.readdirSync(SF_PATH, { withFileTypes: true })
  filenames = allFiles
    .filter(dirent => (dirent.isFile() && dirent.name.endsWith('.log')))
    .map(dirent => dirent.name)

  filenames.forEach(async _file => {
    if (_file.endsWith('.log')) {
      _file=_file.slice(0, -4)
    }
    
    await reshape(_file)
    calumise(`${_file}_reshape`)
  })
})()


const path = require('path')
const fs = require('fs')
const OS = require('os')
const logger = require('@natfaulk/supersimplelogger')('Reshape')
const mkdir_p = require('../js/utils').mkdir_p

const RESHAPE_PATH = path.join(__dirname, '..', 'outputs', 'reshape')

let reshape = _file => {
  if (_file.endsWith('.log')) {
    _file=_file.slice(0, -4)
  }

  const SF_PATH = path.join(__dirname, '..', 'outputs', `${_file}.log`)
  const OUT_PATH = path.join(RESHAPE_PATH, `${_file}_reshape.log`)
  
  logger(`Reshaping ${SF_PATH}...`)

  if (!fs.existsSync(SF_PATH)) {
    logger(`Could not find sf file ${SF_PATH}`)
    return
  }

  let dataRaw = fs.readFileSync(SF_PATH, 'utf8').split('\n')
  let dataheader = null
  let data = []
  
  dataRaw.forEach(_line => {
    if (_line.length > 2) {
      let parseddata = {}
      // console.log(_line)
      try {
        parseddata = JSON.parse(_line)
      } catch(e) {
        logger('[Loader] Error parsing file')
      }
      if (parseddata !== {}) {
        if (parseddata.type === 'header') {
          dataheader = parseddata
          logger(`[Loader] Smartfloor save file version: ${parseddata.version}`)
        }
        
        if (parseddata.type === 'reading') data.push(parseddata)
      }
    }
  })
  
  dataheader.bb.w = 8
  dataheader.bb.h = 1
  dataheader.tiles.forEach(_t => {
    if (_t.pos.y === 1) {
      _t.pos.x += 4
      _t.pos.y = 0
    }
  })

  let outFile = fs.createWriteStream(OUT_PATH, err => {
    if (err) logger('Error creating write stream')
  })

  outFile.write(JSON.stringify(dataheader))
  outFile.write(OS.EOL)
  
  for (let data_i = 0; data_i < data.length; data_i++) {
    let tempData = data[data_i].data  
    let newData = []
    for (let i = 0; i < 40; i++) {
      newData.push([])
    }
    
    for (let x = 0; x < 20; x++) {
      for (let y = 0; y < 10; y++) {
        if (y < 5) newData[x].push(tempData[x][y])
        else newData[x+20].push(tempData[x][y])
      }
    }
  
    data[data_i].data = newData
    
    outFile.write(JSON.stringify(data[data_i]))
    outFile.write(OS.EOL)
  }

  return new Promise((resolve, reject) => {
    outFile.on('close', ()=>{
      logger(`Reshaped file written to ${OUT_PATH}`)
      resolve()
    })
    
    outFile.end()
  })
}

;(()=>{
  mkdir_p(RESHAPE_PATH)

  if (require.main === module) {
    let args = process.argv.slice(2)
    
    if (args.length === 0) {
      logger('Please add the filenames of files to be decoded as arguements!')
      logger('e.g. decodeBinarySave file1 file2')
      return
    }
    
    args.forEach(async _arg => {
      await reshape(_arg)
    })
  }
})()

module.exports=reshape

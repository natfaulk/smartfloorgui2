const path = require('path')
const fs = require('fs')
const OS = require('os')
const logger = require('@natfaulk/supersimplelogger')('Calumise')
const mkdir_p = require('../js/utils').mkdir_p

const CSV_PATH = path.join(__dirname, '..', 'outputs', 'csv')

let calumise = _file => {
  if (_file.endsWith('.log')) {
    _file=_file.slice(0, -4)
  }

  const SF_PATH = path.join(__dirname, '..', 'outputs', 'reshape', `${_file}.log`)
  const OUT_PATH = path.join(CSV_PATH, `${_file}.csv`)

  logger(`Calumising ${SF_PATH}...`)

  if (!fs.existsSync(SF_PATH)) {
    logger(`Could not find sf file ${SF_PATH}`)
    return
  }

  let dataRaw = fs.readFileSync(SF_PATH, 'utf8').split('\n')
  let dataheader = null
  let data = []
  
  dataRaw.forEach(_line => {
    if (_line.length > 2) {
      let parseddata = {}
      // console.log(_line)
      try {
        parseddata = JSON.parse(_line)
      } catch(e) {
        logger('[Loader] Error parsing file')
      }
      if (parseddata !== {}) {
        if (parseddata.type === 'header') {
          dataheader = parseddata
          logger(`[Loader] Smartfloor save file version: ${parseddata.version}`)
        }
        
        if (parseddata.type === 'reading') data.push(parseddata)
      }
    }
  })

  let outFile = fs.createWriteStream(OUT_PATH, err => {
    if (err) logger('Error creating write stream')
  })
  
  const WIDTH = data[0].data.length
  const HEIGHT = data[0].data[0].length
  logger(`Width: ${WIDTH}, Height:${HEIGHT}`)

  outFile.write('time,')
  for (let _x = 0; _x < WIDTH; ++_x) {
    for (let _y = 0; _y < HEIGHT; ++_y) {
      outFile.write(`p${_x}_${_y},`)
    }
  }
  outFile.write(OS.EOL)

  data.forEach(_dat=>{
    outFile.write(`${_dat.time.toString()},`)

    _dat.data.forEach((_d, _ix) => {
      _d.forEach((_d2, _iy) => {
        outFile.write(`${_d2.val},`)
      })
    })
    outFile.write(OS.EOL)
  })

  outFile.end()

  logger(`Calumised file written to ${OUT_PATH}`)
}

;(()=>{
  mkdir_p(CSV_PATH)

  if (require.main === module) {
    let args = process.argv.slice(2)
    
    if (args.length === 0) {
      logger('Please add the filenames of files to be decoded as arguements!')
      logger('e.g. decodeBinarySave file1 file2')
      return
    }
    
    args.forEach(_arg => {
      calumise(_arg)
    })
  }
})()

module.exports=calumise

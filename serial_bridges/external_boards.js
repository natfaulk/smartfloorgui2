const logger = require('@natfaulk/supersimplelogger')('Bridge (external boards)')

const SerialPort = require('serialport')
const http = require('http')
const fs = require('fs')
const path = require('path')

;(() => {
  let settings = parseArguements()
  if (settings === null) return

  let rawdata = fs.readFileSync(path.join(__dirname, 'comports.json'))
  let portNames = JSON.parse(rawdata).external
  let ports = []
  
  logger('Opening:', portNames)

  portNames.forEach(_portname => {
    const port = new SerialPort(_portname, {
      baudRate: 115200
    })

    let portWrapper = {
      port: port,
      buff: ''
    }

    port.on('error', err => {
      logger('Error: ', err.message)
    })

    port.on('data', data => {
      portWrapper.buff += data.toString('utf8')
      // console.log(portWrapper.buff)
      let msgs = portWrapper.buff.split('\n')
      
      while(msgs.length > 1) {
        let latest = msgs.shift()
        sendData(latest, settings)
      }
      if (msgs.length > 0) portWrapper.buff = msgs[0]
    })

    ports.push(portWrapper)
  })
})()

let sendData = (_data, _settings) => {
  let postData = _data

  let options = {
    host: _settings.dest,
    path: '/submit',
    port: _settings.port,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(postData)
    }
  }

  let req = http.request(options, res => {
    let data = ''

    res.on('data', chunk => {
      data += chunk
    })

    res.on('end', () => {
      logger(data);
    })
  }).on('error', err => {
    logger(`Error: ${err.message}`)
  })

  req.write(postData)
}

function displayHelp() {
  console.log('\nFW Bridge Help')
  console.log('----------------\r\n')
  console.log('Set host address:')
  console.log('--host=127.0.0.1 Defaults to 127.0.0.1')
  console.log('Can use domain name ie test.com or ip')
  console.log('Note this input is not validated. If there is a server error check this parameter\n')
  console.log('Set port:')
  console.log('--port=3000 Defaults to 3000\n')
  console.log('Display help:')
  console.log('help or --help or -h\n')
}

function parseArguements() {
  let trimArg = _arg => {
    return _arg.substring(_arg.indexOf('=') + 1)
  }
  
  let settings = {
    dest: '127.0.0.1',
    port: 3000,
  }

  let exitProgram = false

  process.argv.slice(2).forEach(arg => {
    if (arg.startsWith('--host=')) {
      settings.dest = trimArg(arg)
      logger(`Host address set to ${settings.dest}`)
    } else if (arg.startsWith('--port=')) {
      let tempPort = parseInt(trimArg(arg))
      if (isNaN(tempPort)) logger(`Port not a number. Ignoring...`)
      else {
        settings.port = tempPort
        logger(`Port set to ${settings.port}`)
      }
    } else if (arg == 'help' || arg == '--help' || arg == '-h') {
      displayHelp()
      exitProgram = true
    } else logger(`Unknown argument: ${arg}`)
  })

  if (exitProgram) return null
  else return settings
}

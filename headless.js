const path = require('path')
const express = require('express')
const Settings = require('./js/settings')
const mkdir_p = require('./js/utils').mkdir_p
const LayoutCfg = require('./js/layoutCfg')
const Floortiles = require('./js/floortile')
const VERSION = require('./package.json').version

const port = 3000
const RECORD_PREFIX = 'out'

;(() => {
  let state = {
    recording: false
  }

  console.log(`Version: ${VERSION}`)

  console.log('Loading settings...')
  mkdir_p('config')
  let settings = Settings.load()
  
  console.log('Loading layout...')
  let layout = LayoutCfg.loadCfgFile()
  Floortiles.setupTiles(settings, layout)
  

  console.log('Starting server...')
  const ex_app = express()
  ex_app.use(express.json())
  ex_app.use(function (err, req, res, next) {
    if (err) {
      console.log(err)
      // console.log(req)
      res.send('nack')
    }
    else next()
  })

  ex_app.use(express.static('statics'))
  ex_app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'statics', 'headless.html')))
  ex_app.get('/status', (req, res) => res.send(`Recording: ${state.recording}`))
  ex_app.post('/submit', (req, res) => {
    let id = req.body.ID
    // console.log(id)

    Floortiles.updateData(req.body)
    if (state.recording) Floortiles.saveMatrix()

    res.send('ack')
  })
  ex_app.get('/start', (req, res) => {
    if (!state.recording) state.recording = Floortiles.beginRecording(RECORD_PREFIX)
    res.send('ack')
  })
  ex_app.get('/stop', (req, res) => {
    if (state.recording) {
      state.recording = false
      Floortiles.endRecording()
    }
    res.send('ack')
  })

  ex_app.get('/tare', (req, res) => {
    Floortiles.tare()
    res.send('ack')
  })

  ex_app.listen(port, '0.0.0.0', () => console.log(`Example app listening on port ${port}!`))
})()

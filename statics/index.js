let getStatus = async () => {
  let response = await fetch('/status')
  if (response.ok) {
    let text = await response.text()
    let statusDom = document.getElementById('status')
    statusDom.innerHTML = text
  } else {
    console.log("HTTP-Error: " + response.status)
  }
}

let start = async () => {
  setMessage('')
  let response = await fetch('/start')
  if (response.ok) {
    let text = await response.text()
    setMessage(text)

    getStatus()
  } else {
    console.log("HTTP-Error: " + response.status)
    setMessage('Failed')
  }
}

let stop = async () => {
  setMessage('')
  let response = await fetch('/stop')
  if (response.ok) {
    let text = await response.text()
    setMessage(text)
    console.log(text)

    getStatus()
  } else {
    console.log("HTTP-Error: " + response.status)
    setMessage('Failed')
  }
}

let setMessage = _message => {
  let messageDom = document.getElementById('messages')
  messageDom.innerHTML = _message
}

window.onload = getStatus

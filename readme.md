# Sensing Floor GUI
## To run
The app runs as a desktop app using electron.  
[NodeJS](https://nodejs.org/en/) needs to be installed.
Download this repository `git clone https://bitbucket.org/natfaulk/smartfloorgui2.git`  
Install the dependencies (run from inside the skeletonsport folder) `npm install`  
Run the app `npm start`  
  
[Device firmware is here](https://bitbucket.org/natfaulk/smartfloorfw2)
